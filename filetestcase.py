__author__ = 'Alex Prykhodko (Pixellu LLC)'

from unittest import TestCase
from fnmatch import fnmatch
import os
import os.path
import re
import shutil


def _unix_is_file_hidden(file_name):
    """
    Detects if the file name contains a hidden file (starts with a period).
    :param file_name: file name to check.
    :return: True if file is hidden; False otherwise.
    """
    assert len(file_name) > 0
    if file_name not in [".", ".."]:
        return file_name[0] == "."
    else:
        return False


class FileExists(object):
    """
    A class to be used as part of the directory structure
    """
    pass


class DirectoryStructure(dict):
    """
    A dict subclass that is used to defined structure for directory structure tests.
    """

    def flatten(self, ignore_hidden=True, directory_structure=None, path=None):

        if directory_structure is None:
            directory_structure = self

        if path is None:
            path = ""

        if type(directory_structure) is list:
            result = []
            for item in directory_structure:
                if type(item) is not str:
                    raise ValueError("End-point items in directory structure lists must be strings.")
                if not (ignore_hidden and _unix_is_file_hidden(item)):
                    result.append(path + item)
            return result
        elif directory_structure is FileExists:
            return [path]
        elif isinstance(directory_structure, dict):
            result = []
            for key, value in directory_structure.items():
                if not (ignore_hidden and _unix_is_file_hidden(key)):
                    result.extend(self.flatten(ignore_hidden, value, path + key))
            return result

        raise ValueError("Invalid value for directory structure: %s" % str(directory_structure))


class FileTestCase(TestCase):

    MATCH_NONE = 0
    MATCH_ACTUAL = 1
    MATCH_EXPECTED = 2
    SCOPE_ACTUAL = 0
    SCOPE_EXPECTED = 1
    SCOPE_NAMES = ["actual", "expected"]

    def runTest(self):
        """
        Dummy method to prevent Python 2 unittest run errors.
        :return: none.
        """
        pass

    @classmethod
    def _path_pattern_match(cls, actual, expected):

        actual_parts = actual.split(os.sep)
        expected_parts = expected.split(os.sep)

        expected_is_dir = False
        actual_is_dir = False

        if expected[-1] == "/":
            expected_is_dir = True
            del expected_parts[-1]
        if actual[-1] == "/":
            actual_is_dir = True
            del actual_parts[-1]

        match_actual = True
        match_expected = True

        if len(expected_parts) > len(actual_parts):
            match_expected = False

        for i in range(0, len(actual_parts)):

            if i == len(expected_parts) - 1\
                    and actual_parts[i] == expected_parts[i]\
                    and (expected_is_dir ^ actual_is_dir):
                match_expected &= False
                match_actual &= False
                break

            if i >= len(expected_parts):
                match_actual &= False
                break

            elif not fnmatch(actual_parts[i], expected_parts[i]):
                match_expected &= False
                match_actual &= False
                break

        return match_actual, match_expected

    @classmethod
    def _get_directory_structure_matches(cls, path, directory_structure, ignore_hidden):
        expected_flat_directory_structure = set(directory_structure.flatten(ignore_hidden))
        expected_flat_directory_structure = {item: False for item in expected_flat_directory_structure}

        actual_flat_directory_structure = []

        path_re = re.compile('^' + re.escape(path))

        for root, dirs, files in os.walk(path):
            root = path_re.sub("", root)
            if len(root) > 0 and ignore_hidden and _unix_is_file_hidden(root.split(os.sep)[-1]):
                continue
            all_files = [str(root + os.sep + item).lstrip(os.sep) for item in files
                         if not (ignore_hidden and _unix_is_file_hidden(item))]
            all_dirs = [str(root + os.sep + item + os.sep).lstrip(os.sep) for item in dirs
                        if not (ignore_hidden and _unix_is_file_hidden(item))]
            actual_flat_directory_structure.extend(all_dirs)
            actual_flat_directory_structure.extend(all_files)

        actual_flat_directory_structure = {item: False for item in actual_flat_directory_structure}

        for actual_item_path in actual_flat_directory_structure:
            for expected_item_path in expected_flat_directory_structure:
                match_actual, match_expected = cls._path_pattern_match(actual_item_path, expected_item_path)
                expected_flat_directory_structure[expected_item_path] |= match_expected
                actual_flat_directory_structure[actual_item_path] |= match_actual

        return actual_flat_directory_structure, expected_flat_directory_structure

    @classmethod
    def _format_missing(cls, flat_directory_structure, scope):
        result = ""
        for path, present in flat_directory_structure.items():
            if not present:
                result += "    %s is not present in the %s directory structure.\n" % (path, cls.SCOPE_NAMES[scope])
        return result

    def assertFileExists(self, file_path):
        if not os.path.exists(file_path):
            raise AssertionError("File does not exist: %s" % file_path)

    def assertDirectoryStructureMatchesExactly(self, expected_directory_structure, path, ignore_hidden=True):
        """
        Ensures that the director structure under ``path`` matches exactly the ``directory_structure``.
        Extra files present under ``path`` will cause the assertion to fail.
        :param path: path to perform recursive scan on.
        :param expected_directory_structure: directory structure that ``path`` must match exactly.
        :param ignore_hidden: set to False to not ignore hidden files; defaults to True.
        :return: none.
        """
        message = ""
        actual_flat_directory_structure, expected_flat_directory_structure =\
            self._get_directory_structure_matches(path, expected_directory_structure, ignore_hidden=ignore_hidden)

        if False in set(actual_flat_directory_structure.values()):
            message += "Actual directory structure does not match expected directory structure: \n" + \
                self._format_missing(actual_flat_directory_structure, self.SCOPE_ACTUAL)

        if False in set(expected_flat_directory_structure.values()):
            message += "\nExpected directory structure does not match actual directory structure: \n" + \
                self._format_missing(expected_flat_directory_structure, self.SCOPE_EXPECTED)

        if len(message) > 0:
            raise AssertionError(message)

    def assertDirectoryStructurePresent(self, expected_directory_structure, path, ignore_hidden=True):
        """
        Ensures that all files/subdirectories specified in the expected_directory_structure are present under ``path``.
        Extra files present under ``path`` will not cause an assertion failure.
        :param path: path to perform recursive scan on.
        :param expected_directory_structure: directory structure that must exist under ``path``.
        :param ignore_hidden: set to False to not ignore hidden files; defaults to True.
        :return: none.
        """

        message = ""
        actual_flat_directory_structure, expected_flat_directory_structure =\
            self._get_directory_structure_matches(path, expected_directory_structure, ignore_hidden=ignore_hidden)

        if False in set(expected_flat_directory_structure.values()):
            message += "\nExpected directory structure does not match actual directory structure: \n" + \
                self._format_missing(expected_flat_directory_structure, self.SCOPE_EXPECTED)

        if len(message) > 0:
            raise AssertionError(message)


#==============================================================================
# Unit tests
#==============================================================================

class FileTestCaseTests(TestCase):

    UNIT_TEST_DATA_DIR = "filetestcase-unit-test-data"

    def setUp(self):

        assert len(self.UNIT_TEST_DATA_DIR) > 0
        assert self.UNIT_TEST_DATA_DIR != "."
        assert self.UNIT_TEST_DATA_DIR != ".."

        if os.path.exists(os.path.join(self.UNIT_TEST_DATA_DIR)):
            shutil.rmtree(os.path.join(self.UNIT_TEST_DATA_DIR))

        # Initialize the unit tests directory structure:

        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR))
        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1"))
        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "subdirectory1"))
        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "sub-directory2"))
        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "sub-directory2a"))
        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "subdirectory3"))
        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "subdirectory4"))
        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", ".hidden-subdirectory5"))
        os.mkdir(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", ".hidden-subdirectory6"))

        open(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "subdirectory1", "file1.txt"), "w")\
            .write("unit test data")
        open(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "sub-directory2", "file2.txt"), "w")\
            .write("unit test data")
        open(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "sub-directory2a", "file2.txt"), "w")\
            .write("unit test data")
        open(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", "subdirectory4", "file4.txt"), "w")\
            .write("unit test data")
        open(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", ".hidden-subdirectory5", "file5.txt"), "w")\
            .write("unit test data")
        open(os.path.join(self.UNIT_TEST_DATA_DIR, "directory1", ".hidden-subdirectory6", "file6.txt"), "w")\
            .write("unit test data")

        self.file_tests = FileTestCase()

    def tearDown(self):

        assert len(self.UNIT_TEST_DATA_DIR) > 0
        assert self.UNIT_TEST_DATA_DIR != "."
        assert self.UNIT_TEST_DATA_DIR != ".."

        if os.path.exists(os.path.join(self.UNIT_TEST_DATA_DIR)):
            shutil.rmtree(os.path.join(self.UNIT_TEST_DATA_DIR))


    def testPatternMatching(self):

        self.assertEqual((True, False), FileTestCase._path_pattern_match("directory1/",
                                                                         "directory1/subdirectory3"))
        self.assertEqual((True, True), FileTestCase._path_pattern_match("directory1/subdirectory3",
                                                                        "directory1/subdirectory3"))
        self.assertEqual((False, False), FileTestCase._path_pattern_match("directory1/subdirectory3/",
                                                                          "directory1/subdirectory3"))
        self.assertEqual((False, False), FileTestCase._path_pattern_match("directory1/subdirectory3",
                                                                          "directory1/subdirectory3/"))

        self.assertEqual((False, False), FileTestCase._path_pattern_match("directory1/sub-directory1/",
                                                                          "directory1/subdirectory3"))
        self.assertEqual((False, False), FileTestCase._path_pattern_match("directory1/sub-directory1/",
                                                                          "directory1/subdirectory3/"))
        self.assertEqual((False, False), FileTestCase._path_pattern_match("directory1/sub-directory2/file1.txt",
                                                                          "directory1/subdirectory3"))

    def testDirectoryStructure(self):

        self.maxDiff = None

        directory_structure = DirectoryStructure({
            "directory1/": {
                "subdirectory1/": [
                    "file1.txt"
                ],
                "sub-*/": [
                   "file2.txt"
                ],
                "subdirectory3/": FileExists,
                "subdirectory4/": ["file4.txt"],
            }
        })
        self.file_tests.assertDirectoryStructureMatchesExactly(directory_structure, "./%s" % self.UNIT_TEST_DATA_DIR,
                                                               ignore_hidden=True)

        directory_structure = DirectoryStructure({
            "directory1/": {
                "subdirectory1/": [
                    "file1.txt"
                ],
                "sub-*/": [
                   "file2.txt"
                ],
                "subdirectory3": FileExists,
                ".hidden-subdirectory5/": ["file5.txt"]
            }
        })

        expected_expected = {
            "directory1/sub-*/file2.txt": True,
            "directory1/subdirectory1/file1.txt": True,
            "directory1/subdirectory3": False,
            "directory1/.hidden-subdirectory5/file5.txt": True
        }
        expected_actual = {
            "directory1/": True,
            "directory1/sub-directory2/": True,
            "directory1/sub-directory2/file2.txt": True,
            "directory1/sub-directory2a/": True,
            "directory1/sub-directory2a/file2.txt": True,
            "directory1/subdirectory1/": True,
            "directory1/subdirectory1/file1.txt": True,
            "directory1/subdirectory3/": False,
            "directory1/subdirectory4/": False,
            "directory1/subdirectory4/file4.txt": False,
            "directory1/.hidden-subdirectory5/": True,
            "directory1/.hidden-subdirectory5/file5.txt": True,
            "directory1/.hidden-subdirectory6/": False,
            "directory1/.hidden-subdirectory6/file6.txt": False,
        }

        actual_actual, actual_expected = self.file_tests._get_directory_structure_matches(
            "./%s" % self.UNIT_TEST_DATA_DIR, directory_structure, ignore_hidden=False)
        self.assertEqual(expected_actual, actual_actual)
        self.assertEqual(expected_expected, actual_expected)

        with self.assertRaises(AssertionError):
            self.file_tests.assertDirectoryStructureMatchesExactly(directory_structure,
                                                                   "./%s" % self.UNIT_TEST_DATA_DIR)
