# Python FileTestCase

## Python Version Support

Both Python 2 & 3 versions are supported.

## Usage

First, define an expected directory structure:

    directory_structure = DirectoryStructure({
            "directory1/": {
                "subdirectory1/": [ # Subdirectories are always trailing with a slash.
                    "file1.txt"
                ],
                "sub-*/": [
                   "file2.txt"
                ],
                "subdirectory3/": FileExists, # Just a dummy class.    
                "subdirectory4/": ["file4a.txt", "file4b.txt"]
            }
        })

Note that FileExists is a filler for files or empty subdirectories within a dict. It is important to indicate
subdirectories with a trailing slash (OS-dependent).

This will result in the flattened structure as follows:
    
    [
        "directory1/subdirectory1/file1.txt",
        "directory1/sub-*/file2.txt",
        "directory1/subdirectory3",
        "directory1/subdirectory4/file4a.txt",
        "directory1/subdirectory4/file4b.txt"
    ]

Let's assume that we have directory1 one located at /tmp/directory1. Now you may call the assertion methods:

    assertDirectoryStructurePresent(directory_structure, "/tmp", ignore_hidden=True)
    """Ensures that all files/subdirectories specified in the directory_structure are present under /tmp. Extra files
    present under /tmp will not cause an assertion failure."""
     
    assertDirectoryStructureMatchesExactly(directory_structure, "/tmp", ignore_hidden=True)
    """Ensures that the director structure under /tmp matches exactly the directory_structure. Extra files present
    under /tmp will cause the assertion to fail."""


    