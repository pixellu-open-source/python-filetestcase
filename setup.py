from distutils.core import setup

setup(
    name='filetestcase',
    version='0.1',
    py_modules=['filetestcase'],
    license='Apache License, Version 2.0',
    author='Alex Prykhodko',
    author_email='alex@pixellu.com',
    description='A subclass of unittest.TestCase that has assertions for the existence of files, '
                'directory structure matches.'
)
